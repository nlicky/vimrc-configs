" A gvimrc file by viad.

" Font
set guifont=DejaVu\ Sans\ Mono\ 10
" set guifont=DejaVu\ Sans\ Mono:h10

" Remove the toolbar.
set guioptions-=T

" Remove the tear off menu.
set guioptions-=t

" Use console dialogs instead of popup dialogs.
set guioptions+=c

" Disable beeping and flashing for gvim.
set visualbell t_vb=

" Disable cursor blinking
set guicursor+=a:blinkon0

" Open the gvim window maximized.
set lines=999 columns=999

" Colorize the color column.
" highlight ColorColumn guibg=#3F3F3F    " 75% Gray
