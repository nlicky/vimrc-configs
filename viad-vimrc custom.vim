" A vimrc file by viad.
" This vimrc config uses custom files!
"
" Required files:
" Base16 Default Dark (Colorscheme)
"
" Sections
" ----------
" 1. User/Visuals
" 2. Editing
" 3. Searching
" 4. Commands

" Run in vim mode.
set nocompatible

" Set vim to use 256 colors.
"set t_Co=256


" 1. User/Visuals
" Enable syntax highlighting.
syntax on

" Color scheme
colorscheme base16-default-dark

" Set terminal background as dark.
set background=dark

" Set tab key to put 4 spaces.
set shiftwidth=4
set softtabstop=4
set expandtab

" Show a colored column at 80 characters.
set colorcolumn=80
" highlight ColorColumn ctermbg=7

" Highlight the current line.
set cursorline


" 2. Editing
" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

" Show cursor position.
set ruler

" Use the mouse at all times.
set mouse=a

" Determine filetype based on name and contents.
filetype indent plugin on

" Enable autoindenting.
set autoindent

" Display line numbers.
set number

" Hide the buffer when switching between unsaved files.
set hidden

" Don't redraw the screen when running a macro.
set lazyredraw

" Remove all trailing whitespace.
autocmd BufWritePre * %s/\s\+$//e


" 3. Searching
" Use incremental searching.
set incsearch

" Highlight searches.
set hlsearch

" Case insensitive search, except for capital letters.
set ignorecase
set smartcase


" 4. Commands
" Better command line completion.
set wildmenu

" Show partial commands.
set showcmd

" Always show status line.
set laststatus=2

" Show confirmation on when using a command on unsaved changes.
set confirm

" Remove beeping and visual bell on errors.
set noerrorbells visualbell
set t_vb=

" Time out on key codes.
set notimeout ttimeout ttimeoutlen=100

" Set height of command bar.
set cmdheight=2

" Bash style command completion.
set wildmode=list:longest,full


" 5. Files
" Autoread files when the file is changed.
set autoread

" Set default encoding.
set encoding=utf8

" Disable backups.
"set nobackup
"set nowritebackup
"set noswapfile

" Move backups and swap to a central location. Don't forget to make the tmp folder in $HOME first.
set backupdir=$HOME/tmp//
set directory=$HOME/tmp//


" 6. Folding
" Enable code folding.
set foldenable

" Show all folds open at start.
set foldlevelstart=10

" Lower the amount of fold levels for less folded code.
set foldnestmax=10

" Fold based on indentation.
set foldmethod=indent
